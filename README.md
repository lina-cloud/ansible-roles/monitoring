monitoring
=========

Installs Telegraf on ubuntu

Requirements
------------

See Role Variables

Role Variables
--------------

`influx_url` is required
`influx_database` is required
`influx_username` is required
`influx_password` is required


Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: monitoring
           vars:
             influx_url: https://influx.exsample.com
             influx_database: monitoring
             influx_username: monitoringwrite
             influx_password: maybebettterinanansiblevault

License
-------

Mozilla Public License 2.0
